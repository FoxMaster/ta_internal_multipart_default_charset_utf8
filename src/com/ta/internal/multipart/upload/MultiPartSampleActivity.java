package com.ta.internal.multipart.upload;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

/**
 * Sample class to demonstrate the api uses
 * @author vinay
 *
 */
public class MultiPartSampleActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
		
//		UploaderTask.MSG_CONNECTION_ERROR="Set your custome message here";
//		UploaderTask.MSG_FILE_NOT_FOUND="set your custome messge here for file not found";
		
		String url="http://api.development.mastergolf.cn/v1/asking/questions";
		String pathParamKey="video_path";
		String filePath=Environment.getExternalStorageDirectory()+"/1402206811989.jpg";
		String file1Path=Environment.getExternalStorageDirectory()+"/1403166789966.jpg";
		String file2Path=Environment.getExternalStorageDirectory()+"/1403167154289.jpg";
		List<UploadParams> fileparams=new ArrayList<UploadParams>();
		//file 1 (img or video)
		fileparams.add(addParam("image_1",filePath));
		//file 2(img or video)
		fileparams.add(addParam("image_2",file1Path));
		//file 3(img or video)
		fileparams.add(addParam("image_3",file2Path));
		
		//add additional params one by one
		List<UploadParams> moreparams=new ArrayList<UploadParams>();
		
		//example param 1
		moreparams.add(addParam("user_uuid","123"));
//		//example param 2
//		moreparams.add(addParam("token","10"));
		moreparams.add(addParam("title","10"));
		moreparams.add(addParam("content","10"));
		//add as many params you need
		
		TaMultiPartUploader tamultipartuploader = new TaMultiPartUploader(HttpType.HttpPost,url,fileparams,moreparams) {
			
			@Override
			public void onUploadFaild(/*String path,*/ String message)
			{
				// TODO Auto-generated method stub
				Log.e("Message:",message);
				showMessageDialog(MultiPartSampleActivity.this, message);
			}
			
			@Override
			public void onSuccessUpload(/*String path,*/ String message)
			{
				// TODO Auto-generated method stub
				//1. you may want to delete the image after upload
				//show alert or toast if required
				Log.e("Message:",message);
				showMessageDialog(MultiPartSampleActivity.this, message);
			}
		};
		tamultipartuploader.exequte();
	}

	void showMessageDialog(final Context context, String message)
	{
		if (context != null)
		{
			String title = "Message";
			new AlertDialog.Builder(context).setTitle(title)
			.setMessage(message)
			.setCancelable(false).setPositiveButton(context.getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int whichButton)
				{
					dialog.dismiss();
				}
			}).create().show();
		}
	}
	
	/**
	 * add a new key value pair 
	 * @param string
	 * @param string2
	 * @return 
	 */
	public UploadParams addParam(String key, String value)
	{
		UploadParams param = new UploadParams();
		param.setParamKey(key);
		param.setParamValue(value);
		return param;
	}

}
